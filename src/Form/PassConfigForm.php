<?php

namespace Drupal\openweather\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

/**
 * Contribute form.
 */
class PassConfigForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'openweather_appid_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['openweather.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $form = parent::buildForm($form, $form_state);
    $form['appid'] = [
      '#type' => 'textfield',
      '#title' => $this->t('App Id'),
      '#required' => TRUE,
      '#description' => $this->t('Create an account at openweathermap.org. Click on your username and choose "My API Keys". Create a new key and copy it here.'),
      '#default_value' => $this->config('openweather.settings')->get('appid'),
    ];
    $form['geonames_username'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Geonames username'),
      '#description' => $this->t('Create an account at www.geonames.org and enable that account for free webservices under account management. Enter the username here. By default this module is connected with the demo account that has a small limit for calls to the webservice. Failures due to reaching the limit are logged in the logs and the block will not appear.'),
      '#default_value' => $this->config('openweather.settings')->get('geonames_username'),
    ];
    $form['cache_duration'] = [
      '#type' => 'textfield',
      '#attributes' => array(
          ' type' => 'number',
      ),
      '#title' => $this->t('Default Cache duration (seconds)'),
      '#description' => $this->t('The api call limited to 1000 calls per day keep the caching duration at least 87 seconds.'),
      '#default_value' => $this->config('openweather.settings')->get('cache_duration'),
    ];

    return $form;
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('openweather.settings')
      ->set('appid', $form_state->getValue('appid'))
      ->set('geonames_username', $form_state->getValue('geonames_username'))
      ->set('cache_duration', $form_state->getValue('cache_duration'))
      ->save();
    parent::submitForm($form, $form_state);
  }

}
