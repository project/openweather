# Open Weather

Open Weather is a module to parse out the weather response from
[OpenWeather](https://openweathermap.org/). The main reason we need
this module because Google had killed it's weather API,
thus rendering the Google Weather module useless. This
module is a nice way for users to get weather in their site now.

For a full description of the module, visit the
[project page](https://www.drupal.org/project/openweather).

Submit bug reports and feature suggestions, or track changes in the
[issue queue](https://www.drupal.org/project/issues/openweather).


## Table of contents

- Module Details
- Requirements
- Recommended modules
- Configuration
- Installation
- Troubleshooting
- FAQ
- Maintainers


## Module Details

Open weather has a few nice features.

- It can handle unlimited cities for weather output.
  Each city will have it's own block which can be customised to
  show whatever weather details you see fit. You can even theme
  this yourself by overriding the theme that comes with the module.
- It also provides the token option of users so that you can make
  use of their location.
- It will also show the forecast on hourly basis with 3 hours of interval.
- It also provides the data for daily forecast in your site, or
  simply display the data anyway you want.


## Requirements

This module requires no modules outside of Drupal core.


## Recommended modules

- [TOKEN](https://www.drupal.org/project/token)
  When enabled user tokens would be available in as an input value for the
  selcted input option for open weather block.


## Installation

Install as you would normally install a contributed Drupal module. For further
information, see
[Installing Drupal Modules](https://www.drupal.org/docs/extending-drupal/installing-drupal-modules).


## Configuration

1. The APPID which is required to make use of this module can be get from
  openweathermap.org and it is configurable and corresponding configuration
  settings are available at admin/config/services/openweather. Enter your APPID
  over here and if the APPID entered is wrong you can get an error which you can
  see under dblog.

2. The Geonames username is required to make use of this module as well. You can
  get it from [GeoNames](http://www.geonames.org/login). Create your account by filling out
  the form. Check your email (make sure to check your spam) for the confirmation
  email. Log in and click on your username to manage your account. Click "Enable
  account for free webservices" link at the bottom of the form.

3. To use this module you can go to admin/structure/block and click on place
  block where you can find a bolck called Open Weather Block.

4. You can find the weather status using following types of input field.
  City name
  City Id
  Zip Code
  Geographic Coordinates
  Select any one of the input option and place the value corresponding to
  selcted input option in text field.

5. You can also take the details of current user and make use of it for that you
  need to add the field under admin/config/people/accounts/fields for the users.

6. If Token module exists then you can make use of the details stored for the
  user and place the token in text field for example: if you have added a field
  called city name for an user and if data is stored for that user then you can
  find that token available under current user menu and you can place a
  placeholder like [current-user:field_city_name] where field_city_name is the
  machine name of the field created for the user.

7. There will be an option for the number of count which is appicable for two
  condition either you want to display the forecast for hourly basis or daily
  basis. In case of hourlyforecast maximum value should be 36 and in case
  of daily forecast maximum value should be 7.

8. You can select an option for what kinf od data you want to display like
  current weather detail or forecast.

9. You can also select all types of data you want to display in that
  block and it is configurable.

10. Warning Message to be shown, if the text field or count field will be empty.


## Troubleshooting

As of now, Open weather module only works if you have used right APPID from
openweathermap.org otherwise it will show an error an error in dblog (if devel
module is enabled).
In case the module is not working properly, you may try:
- Clear the cache
- Reinstall the module after disable and uninstallation.


## FAQ

Q: Does it work without APPID?

A: NO, you must have APPID from openweathermap.org. I would recommend you
download the devel module (https://www.drupal.org/project/devel) and you will
able the see the error in dblog only for development purpose.


## Maintainers

Current maintainers:

- Prashant Kumar - [prashant114606](https://www.drupal.org/user/3380762)
- Allan Chappell - [generalredneck](https://www.drupal.org/u/generalredneck)
- David Rodríguez - [davidjguru](https://www.drupal.org/u/davidjguru)
- Jürgen Haas - [jurgenhaas](https://www.drupal.org/u/jurgenhaas)

This project has been sponsored by:

- [QED42](https://www.drupal.org/qed42)
  QED42 is a web development agency focussed on helping organisations and
  individuals reach their potential, most of our work is in the space of
  publishing, e-commerce, social and enterprise.

Supporting organizations:

- [Four Kitchens](https://www.drupal.org/four-kitchens)
  - Drupal 9 Development and maintenance.

- [QED42](https://www.drupal.org/qed42)
  - Original conception and Drupal 8 development.
